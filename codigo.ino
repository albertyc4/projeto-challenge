#include "secrets.h"
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <MFRC522.h>
#include <esp_now.h>
#include <WiFi.h>
#include <LiquidCrystal_I2C.h>

#define AWS_IOT_PUBLISH_TOPIC   "esp32/pub"
#define AWS_IOT_SUBSCRIBE_TOPIC "esp32/sub"


#define SS_PIN 32
#define RST_PIN 33


const byte  VERDE1 = 25;
const byte  AMARELO1 = 26;
const byte  VERMELHO1 = 27;
const byte  VERDE2 = 14;
const byte  AMARELO2 = 12;
const byte  VERMELHO2 = 13;

String Vermelho1;  
String Verde1; 
String Vermelho2; 
String Verde2;

String ambulancia1;

int contagem = 0;

WiFiClientSecure net = WiFiClientSecure();
PubSubClient client(net);

unsigned long tempo3_last; // 1000

byte soma1 = 0;

int lcdColumns = 16;
int lcdRows = 2;

String IDtag = ""; //Variável que armazenará o ID da Tag
bool Permitido = false; //Variável que verifica a permissão 
//Vetor responsável por armazenar os ID's das Tag's cadastradas
String TagsCadastradas[] = {"772b5bc9","87f360c9"};

MFRC522 LeitorRFID(SS_PIN, RST_PIN);
LiquidCrystal_I2C lcd(0x27, lcdColumns, lcdRows); 

//AWS

void connectAWS()
{
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
 
  Serial.println("Connecting to Wi-Fi");
 
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
 
  // Configure WiFiClientSecure to use the AWS IoT device credentials
  net.setCACert(AWS_CERT_CA);
  net.setCertificate(AWS_CERT_CRT);
  net.setPrivateKey(AWS_CERT_PRIVATE);
 
  // Connect to the MQTT broker on the AWS endpoint we defined earlier
  client.setServer(AWS_IOT_ENDPOINT, 8883);
 
  // Create a message handler
  client.setCallback(messageHandler);
 
  Serial.println("Connecting to AWS IOT");
 
  while (!client.connect(THINGNAME))
  {
    Serial.print(".");
    delay(100);
  }
 
  if (!client.connected()){
    Serial.println("AWS IoT Timeout!");
    return;
  }
 
  // Subscribe to a topic
  client.subscribe(AWS_IOT_SUBSCRIBE_TOPIC);
 
  Serial.println("AWS IoT Connected!");
}

void cont(){
contagem = contagem;
contagem = contagem + 1;
}
void publishMessage()
{

    Vermelho1 = "Desligado";
    Verde1 = "Ligado";
    Vermelho2 = "Ligado";
    Verde2 = "Desligado";
    cont();
      
  StaticJsonDocument<200> doc;
  doc["Contagem"] = contagem;
  doc["Acesso Liberado"] = ambulancia1;
  doc["Botupuca Vermelho"] = Vermelho1;
  doc["Botupuca Verde"] = Verde1;
  doc["Lins Vermelho"] = Vermelho2;
  doc["Lins Verde"] = Verde2;
  char jsonBuffer[512];
  serializeJson(doc, jsonBuffer); // print to client

  client.publish(AWS_IOT_PUBLISH_TOPIC, jsonBuffer);
}

void publishMessageStatus()
{
  StaticJsonDocument<200> doc;
  doc["Botupuca Vermelho"] = Vermelho1;
  doc["Botupuca Verde"] = Verde1;
  doc["Lins Vermelho"] = Vermelho2;
  doc["Lins Verde"] = Verde2;
  char jsonBuffer[512];
  serializeJson(doc, jsonBuffer); // print to client

  client.publish(AWS_IOT_PUBLISH_TOPIC, jsonBuffer);
}
 
void messageHandler(char* topic, byte* payload, unsigned int length){
  Serial.print("incoming: ");
  Serial.println(topic);
 
  StaticJsonDocument<200> doc;
  deserializeJson(doc, payload);
  const char* message = doc["message"];
  Serial.println(message);
 }

void image5() {
lcd.clear();

byte image09[8] = {B11000, B11000, B00000, B00000, B00000, B00000, B00000, B11000};
byte image08[8] = {B11111, B11111, B00000, B00000, B00000, B00000, B00000, B11111};
byte image07[8] = {B00011, B00011, B00011, B00011, B00011, B00011, B00011, B00011};
byte image23[8] = {B00011, B00000, B00000, B00000, B00000, B00000, B00011, B00011};
byte image24[8] = {B11111, B00000, B00000, B00000, B00000, B00000, B11111, B11111};
byte image25[8] = {B11000, B11000, B11000, B11000, B11000, B11000, B11000, B11000};


lcd.createChar(0, image09);
lcd.createChar(1, image08);
lcd.createChar(2, image07);
lcd.createChar(3, image23);
lcd.createChar(4, image24);
lcd.createChar(5, image25);


lcd.setCursor(8, 0);
lcd.write(byte(0));
lcd.setCursor(7, 0);
lcd.write(byte(1));
lcd.setCursor(6, 0);
lcd.write(byte(2));
lcd.setCursor(6, 1);
lcd.write(byte(3));
lcd.setCursor(7, 1);
lcd.write(byte(4));
lcd.setCursor(8, 1);
lcd.write(byte(5));

}

void image4() {
lcd.clear();

byte image25[8] = {B10000, B10000, B10000, B11100, B11100, B10000, B10000, B10000};
byte image09[8] = {B10000, B10000, B10000, B10000, B10000, B10000, B10000, B10000};
byte image08[8] = {B00111, B01111, B01101, B11001, B11001, B10001, B10001, B00001};
byte image24[8] = {B00001, B00001, B00001, B11111, B11111, B00001, B00001, B00001};
byte image07[8] = {B00000, B00000, B00000, B00000, B00000, B00001, B00001, B00011};
byte image23[8] = {B00011, B00110, B00110, B01111, B01111, B00000, B00000, B00000};


lcd.createChar(0, image25);
lcd.createChar(1, image09);
lcd.createChar(2, image08);
lcd.createChar(3, image24);
lcd.createChar(4, image07);
lcd.createChar(5, image23);


lcd.setCursor(8, 1);
lcd.write(byte(0));
lcd.setCursor(8, 0);
lcd.write(byte(1));
lcd.setCursor(7, 0);
lcd.write(byte(2));
lcd.setCursor(7, 1);
lcd.write(byte(3));
lcd.setCursor(6, 0);
lcd.write(byte(4));
lcd.setCursor(6, 1);
lcd.write(byte(5));

}

void image3() {
lcd.clear();

byte image07[8] = {B00011, B00011, B00000, B00000, B00000, B00000, B00000, B00000};
byte image08[8] = {B11111, B11111, B00000, B00001, B00011, B00110, B01100, B01100};
byte image09[8] = {B11000, B11000, B11000, B10000, B00000, B00000, B00000, B00000};
byte image24[8] = {B00011, B00001, B00000, B00000, B00000, B00001, B11111, B11111};
byte image25[8] = {B10000, B11000, B11000, B11000, B11000, B11000, B10000, B00000};
byte image23[8] = {B00000, B00000, B00000, B00000, B00000, B00000, B00011, B00011};


lcd.createChar(0, image07);
lcd.createChar(1, image08);
lcd.createChar(2, image09);
lcd.createChar(3, image24);
lcd.createChar(4, image25);
lcd.createChar(5, image23);


lcd.setCursor(6, 0);
lcd.write(byte(0));
lcd.setCursor(7, 0);
lcd.write(byte(1));
lcd.setCursor(8, 0);
lcd.write(byte(2));
lcd.setCursor(7, 1);
lcd.write(byte(3));
lcd.setCursor(8, 1);
lcd.write(byte(4));
lcd.setCursor(6, 1);
lcd.write(byte(5));

}

void image2() {
lcd.clear();

byte image08[8] = {B11111, B11111, B00000, B00000, B00000, B00000, B00000, B00001};
byte image07[8] = {B00000, B00001, B00011, B00011, B00000, B00000, B00000, B00000};
byte image09[8] = {B00000, B10000, B11000, B11000, B11000, B11000, B11000, B10000};
byte image24[8] = {B00011, B00110, B01100, B11000, B10000, B00000, B11111, B11111};
byte image23[8] = {B00000, B00000, B00000, B00000, B00001, B00011, B00011, B00011};
byte image25[8] = {B00000, B00000, B00000, B00000, B00000, B00000, B11000, B11000};


lcd.createChar(0, image08);
lcd.createChar(1, image07);
lcd.createChar(2, image09);
lcd.createChar(3, image24);
lcd.createChar(4, image23);
lcd.createChar(5, image25);


lcd.setCursor(7, 0);
lcd.write(byte(0));
lcd.setCursor(6, 0);
lcd.write(byte(1));
lcd.setCursor(8, 0);
lcd.write(byte(2));
lcd.setCursor(7, 1);
lcd.write(byte(3));
lcd.setCursor(6, 1);
lcd.write(byte(4));
lcd.setCursor(8, 1);
lcd.write(byte(5));

}

void image1() {
lcd.clear();

byte image08[8] = {B00111, B01111, B11111, B11111, B10111, B00111, B00111, B00111};
byte image24[8] = {B00111, B00111, B00111, B00111, B00111, B00111, B00111, B00111};
byte image07[8] = {B00000, B00000, B00000, B00000, B00001, B00001, B00001, B00000};


lcd.createChar(0, image08);
lcd.createChar(1, image24);
lcd.createChar(2, image07);


lcd.setCursor(7, 0);
lcd.write(byte(0));
lcd.setCursor(7, 1);
lcd.write(byte(1));
lcd.setCursor(6, 0);
lcd.write(byte(2));

}

void image0() {
lcd.clear();

byte image08[8] = {B11111, B11111, B00000, B00000, B00000, B00000, B00000, B00000};
byte image07[8] = {B00011, B00011, B00011, B00011, B00011, B00011, B00011, B00011};
byte image09[8] = {B11000, B11000, B11000, B11000, B11000, B11000, B11000, B11000};
byte image23[8] = {B00011, B00011, B00011, B00011, B00011, B00011, B00011, B00011};
byte image24[8] = {B00000, B00000, B00000, B00000, B00000, B00000, B11111, B11111};
byte image25[8] = {B11000, B11000, B11000, B11000, B11000, B11000, B11000, B11000};


lcd.createChar(0, image08);
lcd.createChar(1, image07);
lcd.createChar(2, image09);
lcd.createChar(3, image23);
lcd.createChar(4, image24);
lcd.createChar(5, image25);


lcd.setCursor(7, 0);
lcd.write(byte(0));
lcd.setCursor(6, 0);
lcd.write(byte(1));
lcd.setCursor(8, 0);
lcd.write(byte(2));
lcd.setCursor(6, 1);
lcd.write(byte(3));
lcd.setCursor(7, 1);
lcd.write(byte(4));
lcd.setCursor(8, 1);
lcd.write(byte(5));

}
void imageCruz() {
lcd.clear();

byte image08[8] = {B01110, B01110, B01110, B01110, B01110, B01110, B11111, B11111};
byte image24[8] = {B11111, B11111, B01110, B01110, B01110, B01110, B01110, B01110};
byte image25[8] = {B11111, B11111, B00000, B00000, B00000, B00000, B00000, B00000};
byte image09[8] = {B00000, B00000, B00000, B00000, B00000, B00000, B11111, B11111};
byte image07[8] = {B00000, B00000, B00000, B00000, B00000, B00000, B01111, B01111};
byte image23[8] = {B01111, B01111, B00000, B00000, B00000, B00000, B00000, B00000};


lcd.createChar(0, image08);
lcd.createChar(1, image24);
lcd.createChar(2, image25);
lcd.createChar(3, image09);
lcd.createChar(4, image07);
lcd.createChar(5, image23);


lcd.setCursor(7, 0);
lcd.write(byte(0));
lcd.setCursor(7, 1);
lcd.write(byte(1));
lcd.setCursor(8, 1);
lcd.write(byte(2));
lcd.setCursor(8, 0);
lcd.write(byte(3));
lcd.setCursor(6, 0);
lcd.write(byte(4));
lcd.setCursor(6, 1);
lcd.write(byte(5));

}

void setup() {
  Serial.begin(115200);    
  connectAWS();        
  SPI.begin();                    
  LeitorRFID.PCD_Init();
  
  lcd.init();                      
  lcd.backlight();

 
  pinMode(VERDE1, OUTPUT);
  pinMode(AMARELO1, OUTPUT);
  pinMode(VERMELHO1, OUTPUT);
  pinMode(VERDE2, OUTPUT);
  pinMode(AMARELO2, OUTPUT);
  pinMode(VERMELHO2, OUTPUT);
  
  tempo3_last = millis();
}

void loop() {

 lcd.setCursor(0, 0);

 if (millis()-tempo3_last > 1000){
    if (soma1 == 0){
       digitalWrite(VERDE1,HIGH);
       digitalWrite(AMARELO1,LOW);
       digitalWrite(VERMELHO1,LOW);
       digitalWrite(VERDE2,LOW);
       digitalWrite(AMARELO2,LOW);
       digitalWrite(VERMELHO2,HIGH);
image4();
    }   
        else if (soma1 == 1){
image3();
      Serial.println();
      Serial.println("Status semáforo 1:");
      Serial.println("Vermelho: Ligado");
      Serial.println("Verde: Desligado");
      Serial.println();
      Serial.println("Status semáforo 2:");
      Serial.println("Vermelho: Desligado");
      Serial.println("Verde: Ligado");
      Vermelho1 = "Ligado";
      Verde1 = "Desligado";
      Vermelho2 = "Desligado";
      Verde2 = "Ligado";
      publishMessageStatus();
      
    }
  else if (soma1 == 2){
image2();
    }
  else if (soma1 == 3){
image1();
    }
        else if (soma1 == 4){
                digitalWrite(VERDE1,LOW);
                digitalWrite(AMARELO1,HIGH);
image0();
    }
        else if (soma1 == 5){
                digitalWrite(AMARELO1,LOW);
    }
        else if (soma1 == 6){
                digitalWrite(AMARELO1,HIGH);
    }
        else if (soma1 == 7){
                digitalWrite(AMARELO1,LOW);
    }
        else if (soma1 == 8){
                digitalWrite(AMARELO1,HIGH);
    }
        else if (soma1 == 9){
                digitalWrite(AMARELO1,LOW);
    }
    
     else if (soma1 == 10){
       digitalWrite(VERDE1,LOW);
       digitalWrite(AMARELO1,LOW);
       digitalWrite(VERMELHO1,HIGH);
       digitalWrite(VERDE2,HIGH);
       digitalWrite(AMARELO2,LOW);
       digitalWrite(VERMELHO2,LOW); 
image4();   
    }
        else if (soma1 == 11){
image3();
      Serial.println();
      Serial.println("Status semáforo 1:");
      Serial.println("Vermelho: Desligado");
      Serial.println("Verde: Ligado");
      Serial.println();
      Serial.println("Status semáforo 2:");
      Serial.println("Vermelho: Ligado");
      Serial.println("Verde: Desligado");
      Vermelho1 = "Desligado";
      Verde1 = "Ligado";
      Vermelho2 = "Ligado";
      Verde2 = "Desligado";
      publishMessageStatus();
    }
  else if (soma1 == 12){
image2();
    }
  else if (soma1 == 13){
image1();
    }
        else if (soma1 == 14){
                digitalWrite(AMARELO2,HIGH);
                digitalWrite(VERDE2,LOW);
image0();
    }
        else if (soma1 == 15){
                digitalWrite(AMARELO2,LOW);
    }
        else if (soma1 == 16){
                digitalWrite(AMARELO2,HIGH);
    }
        else if (soma1 == 17){
                digitalWrite(AMARELO2,LOW);
    }
        else if (soma1 == 18){
                digitalWrite(AMARELO2,HIGH);
    }
        else if (soma1 == 19){
                digitalWrite(AMARELO2,LOW);
    }
    
     soma1 = soma1 + 1;
     if (soma1 == 20){
       soma1 = 0;
     }
    tempo3_last = millis();
  }
  Leitura();
}

void Leitura(){
        IDtag = ""; //Inicialmente IDtag deve estar vazia.
        
        // Verifica se existe uma Tag presente
        if ( !LeitorRFID.PICC_IsNewCardPresent() || !LeitorRFID.PICC_ReadCardSerial() ) {
            delay(50);
            return;
        }
        
        // Pega o ID da Tag através da função LeitorRFID.uid e Armazena o ID na variável IDtag        
        for (byte i = 0; i < LeitorRFID.uid.size; i++) {        
            IDtag.concat(String(LeitorRFID.uid.uidByte[i], HEX));
        }        
        
        //Compara o valor do ID lido com os IDs armazenados no vetor TagsCadastradas[]
        for (int i = 0; i < (sizeof(TagsCadastradas)/sizeof(String)); i++) {
          if(  IDtag.equalsIgnoreCase(TagsCadastradas[i])  ){
              Permitido = true; //Variável Permitido assume valor verdadeiro caso o ID Lido esteja cadastrado
          }
        }       
        if(Permitido == true) acessoLiberado(); //Se a variável Permitido for verdadeira será chamada a função acessoLiberado() 
        else acessoNegado(); //Se não será chamada a função acessoNegado()       
}

void acessoLiberado(){
  Serial.println("Tag Cadastrada: " + IDtag); //Exibe a mensagem "Tag Cadastrada" e o ID da tag não cadastrada
  if (IDtag == "772b5bc9")
  {
    ambulancia1 = IDtag + ": XZV-76G9";
  }
   if (IDtag == "87f360c9")
  {
    ambulancia1 = IDtag + ": ABC-86G3";
  }
  
  publishMessage();
  efeitoPermitido();  //Chama a função efeitoPermitido()
}
void acessoNegado(){
  Serial.println("Tag NAO Cadastrada: " + IDtag); //Exibe a mensagem "Tag NAO Cadastrada" e o ID da tag cadastrada
}


void efeitoPermitido(){
      Serial.println();
      Serial.println("Status semáforo 1:");
      Serial.println("Vermelho: Desligado");
      Serial.println("Verde: Ligado");
      Serial.println();
      Serial.println("Status semáforo 2:");
      Serial.println("Vermelho: Ligado");
      Serial.println("Verde: Desligado");
    digitalWrite(VERDE1,HIGH);
    digitalWrite(AMARELO1,LOW);
    digitalWrite(VERMELHO1,LOW);
    digitalWrite(VERDE2,LOW);
    digitalWrite(AMARELO2,LOW);
    digitalWrite(VERMELHO2,HIGH);
    imageCruz();
    delay(21000);
    soma1 = 0;
}
